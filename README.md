# 4988 Stepper Driver for FlashForge Dreamer

I'm sure as **heck** not paying 16-28€ a piece for something I can make myself. (Stepper motor driver for FF's **Dreamer** 3D-printer)

# Parts

[Allegro MicroSystems A4988SETTR-T](https://www.digikey.fi/en/products/detail/allegro-microsystems/A4988SETTR-T/2237991)

# Maintainer

[Elmo Rohula](https://gitlab.com/rootElmo)
